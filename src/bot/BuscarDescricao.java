package bot;

/**
 * Classe responsável por buscar um Bem de acordo com a Descrição
 * */
public class BuscarDescricao extends  Buscar{

    /**
     * Busca um Bem de acordo com sua descrição.
     * @param item a descrição do Bem que deseja encontrar.
     * @return um <code>Bem</code> correspondente ao Bem sendo buscado.
     * */
    public Bem buscar(String item){
        for (Bem bem : bens ) {

            if(bem.getDescricao().equals(item)) {
                return bem;
            }
        }
        return null;
    }
}
