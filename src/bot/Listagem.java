package bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;

public interface Listagem {
    Gerenciador gerenciador = Gerenciador.getInstance();

    public void listar(TelegramBot bot, Update update);
}
