package bot;

/**
 * Essa classe contém informações necessárias para Localização
 * */

public class Localizacao {
    private String nome;
    private String descricao;

    // --------------------------------------------- CONSTRUTOR

    /**
     * Construtor Localizacao
     * @param nome é o nome da Localização
     * @param descricao  é a descrição da Localização
     * */
    public Localizacao(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    // --------------------------------------------- GETTER AND SETTER

    /**
     * Obtém o nome da Localização.
     * @return retorna uma <code>String</code> correspondente ao nome da Localização
     * */
    public String getNome() {
        return nome;
    }

    /**
     * Configura o nome da Localização
     * @param nome  o nome da Localização
     * */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Obtém a descrição da Localização
     * @return retorna uma <code>String</code> correspondente a uma Localização
     * */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Configura a descrição da Localização
     * @param descricao a descrição da Localização
     * */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
