package bot;

import java.util.*;

//Singleton
/**
 * Classe que gerencia as infromações adicionadas. É ela que contem os arrays das Localizações, Categorias e Bens.
 * */
public class Gerenciador {

    private static ArrayList<Localizacao> listaLocalizacao = new ArrayList();
    private static ArrayList<Categoria> listaCategoria = new ArrayList();
    private static ArrayList<Bem> listaBem = new ArrayList();

    private static Gerenciador instance = new Gerenciador();

    /**
     * Adiciona uma Localização ao ArrayList de Localizações.
     * @param localizacao a Localização a ser adicionada.
     * */
    public void adicionarLocalizacao(Localizacao localizacao){
        listaLocalizacao.add(localizacao);
    }

    /**
     * Adiciona uma Categoria ao ArrayList de Categoria.
     * @param categoria a Categoria a ser adicionada.
     * */
    public void adicionarCategoria (Categoria categoria){
        listaCategoria.add(categoria);
    }

    /**
     * Adiciona um Bem ao ArrayList de Bem.
     * @param bem o Bem a ser adicionado.
     * */
    public void adicionarBem (Bem bem){
        listaBem.add(bem);
    }

    /**
     * Movimenta um Bem de uma Localização para outra.
     * @param codigoBem o codigo do Bem a ser movimentado
     * @param nomeLocalizacao o nome da nova Localização.
     * @param descricaoLocalizacao  a descrição da nova Localização.
     * */
    public void moverBem (String codigoBem, String nomeLocalizacao, String descricaoLocalizacao){
        for (Bem bemMovimentacao : listaBem ) {
            if (bemMovimentacao.getCodigo().equals(codigoBem)){
                for (Localizacao novaLocalizacao : listaLocalizacao){
                    if (novaLocalizacao.getNome().equals(nomeLocalizacao) && novaLocalizacao.getDescricao().equals(descricaoLocalizacao)){
                        bemMovimentacao.setLocalizacao(novaLocalizacao);
                    }else{
                        System.out.println("Localização não cadastrada");
                    }
                }
            }else{
                System.out.println("Bem não cadastrado");
            }

        }
    }

    /**
     * Ordena o ArrayList de Bens de acordo com o nome dos Bens.
     *  @return um <code>ArrayList<Bem></></code> correspondente a uma Lista de Bens.
     * */
    public ArrayList<Bem> ordenacaoNome (){
        ArrayList<Bem> bemOrdenado = listaBem;
        Collections.sort(bemOrdenado, Bem.ComparadorNome);

        return  bemOrdenado;

    }

    /**
     * Ordena o ArrayList de Bens de acordo com o nome da Categoria dos Bens.
     * @return um <code>ArrayList<Bem></></code> correspondente a uma Lista de Bens.
     * */
    public ArrayList<Bem> ordenacaoCategoria (){
        ArrayList<Bem> bemOrdenado = listaBem;
        Collections.sort(bemOrdenado, Bem.ComparadorCategoria);

        return bemOrdenado;

    }

    /**
     * Ordena o ArrayList de Bens de acordo com o nome da Localização dos Bens.
     * @return um <code>ArrayList<Bem></></code> correspondente a uma Lista de Bens.
     * */

    public ArrayList<Bem> ordenacaoLocalizacao (){
        ArrayList<Bem> bemOrdenado = listaBem;
        Collections.sort(bemOrdenado, Bem.ComparadorLocalizacao);

        return bemOrdenado;
    }

    // --------------------------------------------- GETTER

    /**
     * Obtém a instancia de Gerenciador
     * @return um <code>Gerenciador</code> correspondente ao Gerenciador inicializado.
     * */
    public static  Gerenciador getInstance(){
        return  instance;
    }

    /**
     * Obtém a lista de Localizações
     * @return um <code>ArrayList<Localizacao></></code> correspondente a uma Lista de Localizações.
     * */
    public ArrayList<Localizacao> getListaLocalizacao() {
        return listaLocalizacao;
    }

    /**
     * Obtém uma lista de Categorias.
     * @return um <code>ArrayList<Categoria></></code> correspondente a uma Lista de Categorias.
     * */
    public ArrayList<Categoria> getListaCategoria() {
        return listaCategoria;
    }

    /**
     * Obtém uma lista de Bens.
     * @return um <code>ArrayList<Bem></></code> correspondente a uma Lista de Bens.
     * */
    public ArrayList<Bem> getListaBem() {
        return listaBem;
    }
}
