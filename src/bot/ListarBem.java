package bot;

import com.pengrad.telegrambot.request.SendMessage;

import java.util.ArrayList;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.response.SendResponse;

/*
* // Criação do objeto bot com as informações de acesso
		TelegramBot bot = TelegramBotAdapter.build("995716781:AAFjTeUgDlyxsvHLkGV77DKqSsR7XPAXJ0A");
		// objeto responsável por receber as mensagens
		GetUpdatesResponse updatesResponse;
		// objeto responsável por gerenciar o envio de respostas
		SendResponse sendResponse;
		// objeto responsável por gerenciar o envio de ações do chat
		BaseResponse baseResponse;
* */

/**
 * Classe responsável pela listagem de Bens
 * */
public class ListarBem implements Listagem {
	ArrayList<Bem> lista = new ArrayList();
	Gerenciador gerenciador = Gerenciador.getInstance();

	/**
	 * Lista os Bens da lista contida na classe Gerenciador
	 * @param bot o bot para qual a mensagem será enviada.
	 * @param update update usado para enviar nova mensagem ao bot.
	 * */
	public void listar(TelegramBot bot, Update update) {

		SendResponse sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Lista de Bens\n"));

		 lista = gerenciador.getListaBem();

		for (Bem bemItem : lista) {
			sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Nome do bem: " + bemItem.getNome()
					+ "\n Código Bem :" + bemItem.getCodigo()
					+ "\n Descrição do bem: " + bemItem.getDescricao()
					+ "\n Categoria do bem: " + bemItem.getCategoria().getNome() + " " + bemItem.getCategoria().getCodigo()
					+ "\n Localização do bem: " + bemItem.getLocalizacao().getNome() +  " " + bemItem.getLocalizacao().getDescricao()));

		}
	}
}
