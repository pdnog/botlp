package bot;

/**
 * Essa classe contém informações necessárias para uma Categoria
 * */

public class Categoria {

    private String nome;
    private  String codigo;
    private String descricao;

    // --------------------------------------------- CONSTRUTOR

    /**
     * Construtor Categoria
     * @param nome é o nome da Categoria
     * @param codigo é o código da Categoria
     * @param descricao é a descrição da Categoria
     * */

    public Categoria(String nome, String codigo, String descricao) {
        this.nome = nome;
        this.codigo = codigo;
        this.descricao = descricao;
    }

    // --------------------------------------------- GETTER AND SETTER

    /**
     * Obtém o nome da Categoria.
     * @return uma <code>String</code> correspondente ao nome da Categoria.
     * */
    public String getNome() {
        return nome;
    }

    /**
     * Configura o nome da Categoria.
     * @param nome o nome da Categoria.
     * */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Obtém o código da Categoria.
     * @return  uma <code>String</code> correspondente ao código da Categoria.
     * */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Configura o código da Categoria.
     * @param codigo o código da Categoria
     * */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Obtém a descrição da Categoria
     * @return  uma <code>String</code> correspondente à descrição da Categoria
     * */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Configura a descrição da Categoria
     * @param descricao a descrição da Categoria
     * */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
