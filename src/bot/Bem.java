package bot;

import java.util.Comparator;
/**
 * Essa classe contém informações de um Bem de um patrimônio.
 * */

public class Bem {
    private String codigo;
    private String nome;
    private String descricao;
    private Localizacao localizacao;
    private Categoria categoria;

    // --------------------------------------------- CONSTRUTOR

    /**
     * Construtor Bem
     * @param  codigo é o código do Bem
     * @param  nome é o nome do Bem
     * @param descricao é a descrição do Bem
     * @param localizacao onde o Bem está localizado
     * @param categoria é a categoria em que o Bem esta
     * */
    public Bem(String codigo, String nome, String descricao, Localizacao localizacao, Categoria categoria) {
        this.codigo = codigo;
        this.nome = nome;
        this.descricao = descricao;
        this.localizacao = localizacao;
        this.categoria = categoria;
    }

    //--------------------------------------------- METHODS - COMPARATORS

    /**
     * Usado para comparar objetos do tipo Bem conforme os seus nomes.
     * */

    public static Comparator<Bem> ComparadorNome = new Comparator<Bem>() {
        @Override
        public int compare(Bem o1, Bem o2) {
            String nomeBem1 = o1.getNome().toUpperCase();
            String nomeBem2 = o2.getNome().toUpperCase();

            return nomeBem1.compareTo(nomeBem2);
        }
    };

    /**
     * Usado para comparar objetos do tipo Bem conforme o nome da categoria.
     * */

    public static Comparator<Bem> ComparadorCategoria = new Comparator<Bem>() {
        @Override
        public int compare(Bem o1, Bem o2) {
            String categoriaBem1 = o1.getCategoria().getNome().toUpperCase();
            String categoriaBem2 = o2.getCategoria().getNome().toUpperCase();

            return categoriaBem1.compareTo(categoriaBem2);
        }
    };

    /**
     * Usado para comparar objetos do tipo Bem conforme o nome da localização.
     * */

    public static Comparator<Bem> ComparadorLocalizacao = new Comparator<Bem>() {
        @Override
        public int compare(Bem o1, Bem o2) {
            String localizacaoBem1 = o1.getLocalizacao().getNome().toUpperCase();
            String localizacaoBem2 = o2.getLocalizacao().getNome().toUpperCase();

            return localizacaoBem1.compareTo(localizacaoBem2);
        }
    };

    // --------------------------------------------- GETTER AND SETTER

    /**
     * Obtém o código do Bem.
     * @return uma <code>String</code> correspondente ao código do Bem.
     * */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Configura o código do .Bem.
     * @param codigo o código do Bem
     * */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Obtém o nome do Bem.
     * @return uma <code>String</code> correspondente ao nome do Bem.
     * */
    public String getNome() {
        return nome;
    }

    /**
     * Configura o nome do Bem.
     * @param nome  o nome do Bem.
     * */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Obtém a descrição do Bem.
     * @return uma <code>String</code> correspondente a descrição do Bem.
     * */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Configura a descrição do Bem.
     * @param descricao a descrição do Bem
     * */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Obtém a localização do Bem.
     * @return uma <code>Localizacao</code> correspondente a localização do Bem.
     * */
    public Localizacao getLocalizacao() {
        return localizacao;
    }

    /**
     * Configura a localização do Bem.
     * @param localizacao a localização do Bem
     * */
    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    /**
     * Obtém a categoria do Bem.
     * @return  uma <code>Categoria</code> correspondente a categoria do .Bem.
     * */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     * Configura a categoria do Bem.
     * @param categoria a categoria do Bem
     * */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

}
