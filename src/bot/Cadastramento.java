package bot;

/**
 * Classe responsável por cadastrar Bens, Categorias e Localizações nos ArrayList específicos de cada objeto.
 * */

public class Cadastramento {

    Gerenciador gerenciador = Gerenciador.getInstance();

    /**
     * Cadastra (insere) uma Localização no ArrayList de Localização.
     * @param nome o nome da Localização.
     * @param descricao a descrição da Localização.
     * */
    public void cadastrar (String nome, String descricao){
        if (nome.isEmpty() || descricao.isEmpty()){
            System.out.println("Valores inválidos");
        }

        Localizacao localizacao = new Localizacao(nome,descricao);

        gerenciador.adicionarLocalizacao(localizacao);
    }

    /**
     * Cadastra (insere) uma Categoria no ArrayList de Categoria.
     * @param codigo o código da Categoria.
     * @param nome o nome da Categoria.
     * @param descricao a descrição da Categoria.
     * */
    public void cadastrar(String codigo, String nome, String descricao){
        if (codigo.isEmpty() || nome.isEmpty() || descricao.isEmpty()){
            System.out.println("Valores inválidos");
        }

        Categoria categoria = new Categoria(nome,codigo, descricao);

        gerenciador.adicionarCategoria(categoria);
    }

    /**
     * Cadsatra (insere) um Bem no ArrayList de Bem.
     * @param codigo o codigo do  Bem.
     * @param nome o nome do Bem.
     * @param descricao a descrição do Bem.
     * @param localizacao a Localização do Bem.
     * @param categoria a Categoria do Bem.
     * */
    public void cadastrar(String codigo, String nome, String descricao,Localizacao localizacao, Categoria categoria){
        if (codigo.isEmpty() || nome.isEmpty() || descricao.isEmpty() || (localizacao == null) || (categoria == null)){
            System.out.println("Valores inválidos");
        }

        Bem bem = new Bem(codigo, nome, descricao, localizacao, categoria);
        gerenciador.adicionarBem(bem);

    }

}
