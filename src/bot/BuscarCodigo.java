package bot;

/**
 * Classe responsável por buscar um Bem de acordo com o código
 * */

public class BuscarCodigo extends  Buscar{

    /**
     * Busca um Bem de acordo com o seu código.
     * @param item o código do Bem que deseja encontrar
     * @return um <code>Bem</code> correspondente ao Bem sendo buscado.
     * */

    public Bem buscar(String item) {
        for (Bem bem : bens ) {

            if(bem.getCodigo().equals(item)) {
                return bem;
            }
        }
        return null;
    }

}
