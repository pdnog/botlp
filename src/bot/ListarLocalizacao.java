package bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;

import java.util.ArrayList;

/**
 * Classe responsável pela listagem de Localizações
 * */
public class ListarLocalizacao implements Listagem {
	ArrayList<Localizacao> lista = new ArrayList();

	/**
	 * Lista as Localizações da lista contida na classe Gerenciador
	 * @param bot o bot para qual a mensagem será enviada.
	 * @param update update usado para enviar nova mensagem ao bot.
	 * */
	public void listar(TelegramBot bot, Update update) {

		SendResponse sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Lista de Localização\n"));

		lista = gerenciador.getListaLocalizacao();

		for (Localizacao itemLocalizacao : lista) {

			sendResponse = bot.execute(new SendMessage(update.message().chat().id(),
					"\n Nome: " + itemLocalizacao.getNome() +
							"\n Descrição: " + itemLocalizacao.getDescricao()));
		}
	}
}
