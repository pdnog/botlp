package bot;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;

import java.util.ArrayList;

/**
 * Classe respopnsável pela listagem de Categorias
 * */
public class ListarCategoria implements Listagem {
	ArrayList<Categoria> lista = new ArrayList();

	/**
	 * Lista as Categorias da lista contida na classe Gerenciador
	 * @param bot o bot para qual a mensagem será enviada.
	 * @param update update usado para enviar nova mensagem ao bot.
	 * */
	public void listar(TelegramBot bot, Update update) {

		SendResponse sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Lista de Categorias\n"));

		lista = gerenciador.getListaCategoria();

		for (Categoria itemCategoria : lista) {

			sendResponse = bot.execute(new SendMessage(update.message().chat().id(),
					"\n Nome: " + itemCategoria.getNome() +
							"\n Código: " + itemCategoria.getCodigo() +
							"\n Descrição: " + itemCategoria.getDescricao()));
		}
	}
}
