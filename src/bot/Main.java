/** @author Anna Beatriz Souza de Albuquerque
 * @author Pedro Avelino Ferreira Nogueira*/
package bot;

import java.util.ArrayList;
import java.util.List;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.ChatAction;
import com.pengrad.telegrambot.request.GetUpdates;
import com.pengrad.telegrambot.request.SendChatAction;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.BaseResponse;
import com.pengrad.telegrambot.response.GetUpdatesResponse;
import com.pengrad.telegrambot.response.SendResponse;

public class Main {

	public static void main(String[] args) {

		// Criação do objeto bot com as informações de acesso
		TelegramBot bot = TelegramBotAdapter.build("995716781:AAFjTeUgDlyxsvHLkGV77DKqSsR7XPAXJ0A");
		// objeto responsável por receber as mensagens
		GetUpdatesResponse updatesResponse;
		// objeto responsável por gerenciar o envio de respostas
		SendResponse sendResponse;
		// objeto responsável por gerenciar o envio de ações do chat
		BaseResponse baseResponse;

		// controle de off-set, isto é, a partir deste ID será lido as mensagens
		// pendentes na fila
		int m = 0;
		boolean solicitarLocalizacao = false;
		boolean solicitarCategoria = false;
		boolean solicitarBem = false;
		boolean buscarNome = false;
		boolean buscarCodigo = false;
		boolean buscarDescricao = false;
		boolean moverBem = false;
		Localizacao localizacao = new Localizacao(null, null);
		Categoria categoria = new Categoria(null, null, null);
		Bem bem = new Bem(null, null, null, null, null);
		Gerenciador gerenciador = Gerenciador.getInstance();

		// loop infinito pode ser alterado por algum timer de intervalo curto
		while (true) {

			// executa comando no Telegram para obter as mensagens pendentes a
			// partir de um off-set (limite inicial)
			updatesResponse = bot.execute(new GetUpdates().limit(100).offset(m));

			// lista de mensagens
			List<Update> updates = updatesResponse.updates();

			// análise de cada ação da mensagem
			for (Update update : updates) {

				// atualização do off-set
				m = update.updateId() + 1;

				System.out.println("Recebendo mensagem:" + update.message().text());

				// envio de "Escrevendo" antes de enviar a resposta
				baseResponse = bot.execute(new SendChatAction(update.message().chat().id(), ChatAction.typing.name()));
				// verificação de ação de chat foi enviada com sucesso
				System.out.println("Resposta de Chat Action Enviada?" + baseResponse.isOk());

				String mensagemRecebida = update.message().text().toString();

				// ========================================================================= CADASTROS
				if (mensagemRecebida.equals("/cadastrolocalizacao") || solicitarLocalizacao) {
					if (localizacao.getNome() == null) {
						sendResponse = bot
								.execute(new SendMessage(update.message().chat().id(), "Cadastro de Localização\n"));

						sendResponse = bot
								.execute(new SendMessage(update.message().chat().id(), "Digite o nome da Localização:"));
						localizacao.setNome(update.message().text());
						solicitarLocalizacao = true;

					} else if (localizacao.getDescricao() == null) {
						localizacao.setNome(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite a descrição da Localização:"));
						localizacao.setDescricao(update.message().text());


					} else if (localizacao.getDescricao() != null) {
						localizacao.setDescricao(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "\n Nome: " + localizacao.getNome() + "\n Descrição: " + localizacao.getDescricao()));
						gerenciador.adicionarLocalizacao(localizacao);
						localizacao = new Localizacao(null, null);
						solicitarLocalizacao = false;
					}

				}

//					 sendResponse = bot.execute(new SendMessage(update.message().chat().id(),"Uma localização aleatória foi cadastrada"));

				else if (mensagemRecebida.equals("/cadastrocategoria") || solicitarCategoria) {
					if (categoria.getNome() == null) {
						sendResponse = bot
								.execute(new SendMessage(update.message().chat().id(), "Cadastro de Categoria\n"));

						sendResponse = bot
								.execute(new SendMessage(update.message().chat().id(), "Digite o nome da Categoria:"));
						categoria.setNome(update.message().text());
						solicitarCategoria = true;

					} else if (categoria.getCodigo() == null) {
						categoria.setNome(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o código da Categoria:"));
						categoria.setCodigo(update.message().text());


					} else if (categoria.getDescricao() == null) {
						categoria.setCodigo(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite a descrição da Categoria:"));
						categoria.setDescricao(update.message().text());


					} else if (categoria.getDescricao() != null) {
						categoria.setDescricao(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "\n Nome: " + categoria.getNome() + "\n Código: " + categoria.getCodigo() + "\n Descrição: " + categoria.getDescricao()));
						gerenciador.adicionarCategoria(categoria);
						categoria = new Categoria(null, null, null);
						solicitarCategoria = false;

					}

				} else if (mensagemRecebida.equals("/cadastrobem") || solicitarBem) {

					if (bem.getNome() == null) {

						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Cadastro de Bem\n"));

						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o nome do Bem:"));
						bem.setNome(update.message().text());
						solicitarBem = true;

					} else if (bem.getCodigo() == null) {
						bem.setNome(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o código do Bem:"));
						bem.setCodigo(update.message().text());


					} else if (bem.getDescricao() == null) {
						bem.setCodigo(update.message().text());
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite a descrição do Bem:"));
						bem.setDescricao(update.message().text());


					} else if (bem.getCategoria() == null) {

						bem.setDescricao(update.message().text());

						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o código da categoria a ser relacionada ao Bem:"));
						bem.setCategoria(new Categoria(null, null, null));
					} else if (bem.getLocalizacao() == null) {

						ArrayList<Categoria> listaCategoria = gerenciador.getListaCategoria();

						for (Categoria itemCategoria : listaCategoria) {
							if (itemCategoria.getCodigo().equals(mensagemRecebida)) {
								bem.setCategoria(itemCategoria);
							}
						}
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite a descrição da localização a ser relacionada ao Bem:"));
						bem.setLocalizacao(new Localizacao(null, null));


					} else if (bem.getLocalizacao() != null) {

						ArrayList<Localizacao> listaLocalizacao = gerenciador.getListaLocalizacao();

						for (Localizacao itemlocalizacao : listaLocalizacao) {
							if (itemlocalizacao.getDescricao().equals(mensagemRecebida)) {
								bem.setLocalizacao(itemlocalizacao);
							}
						}
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "\n Nome: " + bem.getNome() + "\n Codigo: " + bem.getCodigo() + "\n Descrição: " + bem.getDescricao()
																								+ "\n Localização: "+ bem.getLocalizacao().getNome() + " " + bem.getLocalizacao().getDescricao()
																								+ "\n Categoria: "+ bem.getCategoria().getNome() + " " + bem.getCategoria().getCodigo()));
						gerenciador.adicionarBem(bem);
						bem = new Bem(null, null, null, null, null);
						solicitarBem = false;
					}

				}

				// ========================================================================= LISTAR

				else if (mensagemRecebida.equals("/listarbem")) {
					ListarBem listaBem = new ListarBem();
					listaBem.listar(bot, update);

				} else if (mensagemRecebida.equals("/listarcategoria")) {

					ListarCategoria listaCategoria = new ListarCategoria();
					listaCategoria.listar(bot,update);

				} else if (mensagemRecebida.equals("/listarlocalizacao")) {
					ListarLocalizacao listaLocalizacao = new ListarLocalizacao();
					listaLocalizacao.listar(bot, update);
				}
				// ========================================================================= BUSCAR

				else if (mensagemRecebida.equals("/buscarnome") || buscarNome) {
					if (!buscarNome) {
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Busca de Bem por Nome"));
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o nome do Bem:"));

						buscarNome = true;
					} else {
						Buscar busca = new BuscarNome();
						bem.setNome(update.message().text());

						bem = busca.buscar(bem.getNome());

						if (bem != null) {
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(),
									"\n Bem:  " + bem.getNome()
											+ "\n Localização:  " + bem.getLocalizacao().getNome() + " " + bem.getLocalizacao().getDescricao()));
						} else {
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Bem não encontrado"));
						}

						buscarNome= false;
						bem = new Bem (null,null,null,null,null);
						
					}
				} else if (mensagemRecebida.equals("/buscarcodigo") || buscarCodigo) {
					if (!buscarCodigo) {
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Busca de Bem por Código"));
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o código do Bem: "));

						buscarCodigo = true;
					} else {
						Buscar busca = new BuscarCodigo();
						bem.setCodigo(update.message().text());

						bem = busca.buscar(bem.getCodigo());

						if (bem != null) {
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(),
									"\n Bem:  " + bem.getNome()
											+ "\n Localização:  " + bem.getLocalizacao().getNome() + " " + bem.getLocalizacao().getDescricao()));
						} else {
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Bem não encontrado"));
						}

						buscarCodigo = false;
						bem = new Bem (null,null,null,null,null);

					}
				} else if (mensagemRecebida.equals("/buscardescricao") || buscarDescricao) {
					if (!buscarDescricao) {
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Busca de Bem por Descrição"));
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite a descrição do Bem: "));

						buscarDescricao = true;
					} else {
						Buscar busca = new BuscarDescricao();
						bem.setDescricao(update.message().text());

						bem = busca.buscar(bem.getDescricao());

						if (bem != null) {
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(),
									"\n Bem:  " + bem.getNome()
											+ "\n Localização:  " + bem.getLocalizacao().getNome() + " " + bem.getLocalizacao().getDescricao()));
						} else {
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Bem não encontrado"));
						}

						buscarDescricao = false;
						bem = new Bem (null,null,null,null,null);

					}
				}


			// ========================================================================= MOVER

				else if (mensagemRecebida.equals("/moverbem") || moverBem) {
					String nome = null;
					String nomeLocalizacao = null;
					String descricaoLocalizacao = null;

					if (!moverBem){
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Movimentação de Bem"
										+ "\n Digite o código do bem a ser movido: "));
						moverBem = true;
					}else{
						if (bem.getCodigo() == null){
							bem.setCodigo(update.message().text());
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite o nome da  nova localização"));
						}else if (localizacao.getNome() == null){
							localizacao.setNome(update.message().text());
							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Digite a descrição da localização"));
						}else{
							localizacao.setDescricao(update.message().text());

							gerenciador.moverBem(bem.getCodigo(), localizacao.getNome(), localizacao.getDescricao());

							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Movimentação Realizada"));

							sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Bem :" + bem.getCodigo() + " \n Nova Localização: " + localizacao.getNome()
																									+ " " + localizacao.getDescricao() ));

							moverBem = false;
							bem = new Bem(null, null, null, null, null);
							localizacao = new Localizacao(null, null);
						}
					}
				}

				// ========================================================================= GERAR RELATÓRIO

				else if(mensagemRecebida.equals("/gerarrelatorio")){

					// ---------------- LOCALIZAÇÃO

					ArrayList<Bem> listaOrdenada = gerenciador.ordenacaoLocalizacao();
					sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "*️⃣ Listagem de Bens por localizacao"));

					for (Bem bemItem : listaOrdenada) {
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Nome do bem: " + bemItem.getNome()
								+ "\n Código Bem :" + bemItem.getCodigo()
								+ "\n Descrição do bem: " + bemItem.getDescricao()
								+ "\n Categoria do bem: " + bemItem.getCategoria().getNome() + " " + bemItem.getCategoria().getCodigo()
								+ "\n Localização do bem: " + bemItem.getLocalizacao().getNome() +  " " + bemItem.getLocalizacao().getDescricao()));

					}

					// ---------------- CATEGORIA

					listaOrdenada = gerenciador.ordenacaoCategoria();
					sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "*️⃣ Listagem de Bens por categoria"));

					for (Bem bemItem : listaOrdenada) {
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Nome do bem: " + bemItem.getNome()
								+ "\n Código Bem :" + bemItem.getCodigo()
								+ "\n Descrição do bem: " + bemItem.getDescricao()
								+ "\n Categoria do bem: " + bemItem.getCategoria().getNome() + " " + bemItem.getCategoria().getCodigo()
								+ "\n Localização do bem: " + bemItem.getLocalizacao().getNome() +  " " + bemItem.getLocalizacao().getDescricao()));

					}

					// ---------------- NOME

					listaOrdenada = gerenciador.ordenacaoNome();
					sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "*️⃣ Listagem de Bens por nome"));

					for (Bem bemItem : listaOrdenada) {
						sendResponse = bot.execute(new SendMessage(update.message().chat().id(), "Nome do bem: " + bemItem.getNome()
								+ "\n Código Bem :" + bemItem.getCodigo()
								+ "\n Descrição do bem: " + bemItem.getDescricao()
								+ "\n Categoria do bem: " + bemItem.getCategoria().getNome() + " " + bemItem.getCategoria().getCodigo()
								+ "\n Localização do bem: " + bemItem.getLocalizacao().getNome() +  " " + bemItem.getLocalizacao().getDescricao()));

					}



				}

				// ========================================================================= MENU DE OPÇÕES

				else {
					sendResponse = bot.execute(new SendMessage(update.message().chat().id(),
							"🗄🗃📂 Bem vindo ao sistema de controle de Patrimônio 🗂📚📦\n\n"
									+  	" 1⃣ /cadastrolocalizacao para adicionar uma Localização"
									+ "\n 2⃣ /cadastrocategoria para adicionar uma Categoria"
									+ "\n 3⃣ /cadastrobem para adicionar um novo Bem"
									+ "\n 4⃣ /listarlocalizacao para listar as Localizações"
									+ "\n 5⃣ /listarcategoria para listar as Categorias"
									+ "\n 6⃣ /listarbem para listar os Bens"
									+ "\n 7⃣ /buscarnome para buscar Bem pelo nome"
									+ "\n 8⃣ /buscarcodigo para buscar Bem por código"
									+ "\n 9⃣ /buscardescricao para buscar Bem pela descrição"
									+ "\n 🔟 /moverbem para mover um Bem para uma nova localização"
									+ "\n 📄 /gerarrelatorio para gerar um relatório listando todos os bens agrupados por localização, depois categoria e por fim nome"));

				}
			}

			// verificação de mensagem enviada com sucesso
//				System.out.println("Mensagem Enviada?" + sendResponse.isOk());

		}

	}

}