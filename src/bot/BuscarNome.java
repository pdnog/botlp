package bot;

/**
 * Classe responsável por buscar um Bem de acordo com o nome
 * */
public class BuscarNome extends Buscar{

    /**
     * Busca um Bem de acordo com o seu nome.
     * @param item o nome do Bem que deseja encontrar.
     * @return um <code>Bem</code> correspondente ao Bem sendo buscado.
     * */
    public Bem buscar(String item) {
        for (Bem bem : bens ) {

            if(bem.getNome().equals(item)) {
                return bem;
            }
        }
        return null;
    }
}
